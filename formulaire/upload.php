<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = true;
$errorUploadMessage=null;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Test si le fichier est un vrai fichier ou une image bidon
if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($check !== false) {
        echo "Fichier du type - " . $check["mime"] . ".";
        $uploadOk = true;
    } else {
        echo "Ce fichier n'est pas une image.";
        $uploadOk = false;
    }
}

// test si le fichier existe déja
if (file_exists($target_file)) {
    $errorUploadMessage = "Désolé ce fichier existe déja.";
    $uploadOk = false;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    $errorUploadMessage = "Fichier trop gras.";
    $uploadOk = false;
}

// permet uniquement certains formats de fichiers
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
    $errorUploadMessage = "Désolé, seul les JPG, JPEG, PNG  files sont permis.";
    $uploadOk = false;
}

$uploadFile=null;
if ($uploadOk == false) {
    $uploadMessage= "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $uploadMessage = "Le fichier " . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " a été téléchargé.";
        $image=basename($_FILES["fileToUpload"]["name"]);
    } else {
        $uploadMessage= "Sorry, there was an error uploading your file.";
    }
}