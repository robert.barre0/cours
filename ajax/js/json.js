
let utilisateur = {
    "prenom": "Alex",
    "nom": "Dupond",
    "adresse": {
        "rue": "30 avenue du château",
        "ville": "Versailles",
        "cp": 78000,
        "pays": "France"
    },
    "mails": [
        "alex.dupond@gmail.com",
        "alex@dupond.com"
    ]
};

//Conversion en chaine JSON
let json = JSON.stringify(utilisateur);

document.getElementById("resultat").innerHTML =
    "Type de la variable : " + typeof(json) + "<br>Contenu de la variable : " + json;