<?php include("server.php") ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Saisie d'un formulaire</title>
    <link rel="stylesheet" href="css/form.css">
    <script src='js/checkForm.js' async></script>

</head>
<body>
    <div id="container">
        <section>
            <h1>Formulaire de commentaires</h1>
            <form method="post" action="form.php" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="token-12345">
                <div>
                    <label for="firstName">Prénom <span>*</span></label>
                    <input type="text" id="firstName" name="firstName" aria-required="true" required="true">
                </div>
                <div>
                    <label for="lastName">Nom <span>*</span></label>
                    <input type="text" id="lastName" name="lastName">
                </div>
                <div>
                    <label for="message">Le message <span>*</span></label>
                    <textarea id="message" name="message" cols="30" row="10"></textarea>
                </div>

                <div>
                    <label for="fileToUpload">Le fichier </label>
                    <input type="file" name="fileToUpload" id="fileToUpload">
                </div>

                <button type="submit">Enregistrer</button>

            </form>
        </section>

        <?php if (isset($errorUploadMessage) && !is_null($errorUploadMessage)) : ?>
            <div class="alert alert-danger"><?php echo $errorUploadMessage ?></div>
        <?php endif ?>

        <?php if (isset($submitMessage) && !is_null($submitMessage)) : ?>
            <div class="alert alert-info"><?php echo $submitMessage ?></div>
        <?php endif ?>

        <section id="comments_display">
            <?php
            $messages = $pdo->query("SELECT * FROM message")->fetchAll();

            foreach ($messages as $message) {
                echo "<article>";
                echo "<p>" . $message["first_name"] . " " . $message["last_name"] . "</p>";
                echo "<p>" . $message["message"] . "</p>";
                if (!is_null($message["image"]) && $message["image"] !== "") {
                    echo "<img src='upload/" . $message["image"] . "' alt='mon image'/>";
                }
                echo "</article>";
            }
            ?>
        </section>
    </div>
</body>
</html>