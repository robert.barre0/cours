<?php

include "fileUpload.php";

$firstName = isset($_POST["firstName"]) ? htmlspecialchars($_POST["firstName"]) : null;
$lastName = isset($_POST["lastName"]) ? htmlspecialchars($_POST["lastName"]) : null;
$message = isset($_POST["message"]) ?  htmlspecialchars($_POST["message"]) : null;


if (isset($_FILES["fileToUpload"]) && $_FILES["fileToUpload"]["size"]>0) {
    $upload=new fileUpload($_FILES["fileToUpload"]);
}

try {
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'\cours.sqlite');

    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

   $pdo->query(
        'CREATE TABLE IF NOT EXISTS message (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    first_name VARCHAR(50) NOT NULL,
                    last_name VARCHAR(50) NOT NULL,
                    image VARCHAR(255) NOT NULL,
                    message TEXT NOT NULL
                )'
    );
    if ($firstName && $lastName && $message && isset($upload)) {

        $isUploadOK = $upload->isImageValide();
        if (!$isUploadOK) {
            $errorUploadMessage = $upload->getErrorMessage();
        } else {
            if (!$upload->saveFile()) { // on essaie de sauvagarder l'image sur le disque
                $errorUploadMessage = $upload->getErrorMessage();
                $isUploadOK = false;
            }
        }

        if ($isUploadOK) {

            $statement = $pdo->prepare(
                'INSERT INTO message (first_name , last_name, message,image ) VALUES (:first_name, :last_name, :message, :image)'
            );
            $statement->bindValue('first_name', $firstName, PDO::PARAM_STR);
            $statement->bindValue('last_name', $lastName, PDO::PARAM_STR);
            $statement->bindValue('message', $message, PDO::PARAM_STR);
            $statement->bindValue('image', $upload->getName(), PDO::PARAM_STR);

            $statement->execute();

            $submitMessage = "Les informations pour $firstName $lastName ont été sauvegardées";
        }
    } else {
        if (count($_POST) > 0 && !isset($upload)) {
            $errorUploadMessage = "Il faut séléctionner un fichier à télécharger";
        }
    }
    // redirection si nécessaire vers une autre page
  //  header("Location: autre_page.php");

} catch (PDOException $exception) {
    var_dump($exception);
}



