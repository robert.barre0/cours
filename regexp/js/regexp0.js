
let chaine = 'Bonjour, je suis Rackham le Rouge';


let masque1 = /Rackham/;

// Intervalle ou "classe" de caractères permettant de trouver n'importe quelle
// lettre majuscule de l'alphabet classique (sans les accents ou cédille)

let masque2 = /[A-Z]/;
let masque3 = /[A-Z]/g; //Ajout d'une option ou drapeau "global"


//Recherche "Pierre" dans let chaine et renvoie la première correspondance
document.getElementById('p1').innerHTML = 'Trouvé : ' + chaine.match(masque1);

//Recherche toute majuscule dans chaine et renvoie la première correspondance
document.getElementById('p2').innerHTML = 'Trouvé : ' + chaine.match(masque2);

//Recherche toute majuscule dans chaine et renvoie toutes les correspondances
document.getElementById('p3').innerHTML = 'Trouvé : ' + chaine.match(masque3);

/** AUTRE EXEMPLE **/
/*
let masque1 = /Rackham le Rouge/;
let masque2 = /e/;
let masque3 = /ou/g;

let p1 = document.getElementById('p1');
let p2 = document.getElementById('p2');
let p3 = document.getElementById('p3');

p1.innerHTML = chaine.replace(masque1, 'Capitaine Haddock');
p2.innerHTML = chaine.replace(masque2, 'E');
p3.innerHTML = chaine.replace(masque3, 'OU');


 */