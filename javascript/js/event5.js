

//On sélectionne les éléments du document avec les ids d1 et d1p1
let d1 = document.querySelector('#d1');
let d1p1 = document.querySelector('#d1p1');

//On utilise la phase de capture
d1.addEventListener('click', function(){
    alert('div cliqué')}
    );
d1p1.addEventListener('click', cliqueAndStop);

//L'argument 'e' ici sert de référence à un objet Event
function cliqueAndStop(e){
    alert('Paragraphe cliqué - Arrêt de la propagation');
    e.stopPropagation();
}
