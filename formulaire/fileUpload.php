<?php


class fileUpload
{
    private $_size=0;
    private $_mime;
    private $_name;
    private $_tmpName;
    private $_targetFile;

    public $_errorMessage=null;

    const MAX_FILE_SIZE = 5000000;
    const IMAGE_PATH = "upload";

    public function __construct(Array $fileUploadData)
    {
        $this->_size = $fileUploadData["size"];
        $this->_mime = $fileUploadData["type"] ?? null ;
        $this->_name = basename($fileUploadData["name"]);
        $this->_tmpName = $fileUploadData["tmp_name"];

        $this->_targetFile = self::IMAGE_PATH."/".basename($fileUploadData["name"]);
  /*      echo "<pre>";
        print_r($fileUploadData);
        echo "</pre>";
        echo "<hr><pre>";
        print_r($this);
        echo "</pre>";
        exit;*/
    }

    private function testIfImage() {
        return getimagesize($this->_tmpName);
    }

    private function testFileType() {
        $validFormats=["jpg","png","jpeg"];
        $extension = strtolower(pathinfo($this->_name, PATHINFO_EXTENSION));
        return in_array($extension,$validFormats);
    }

    private function testFileSize() {
        return ($this->_size < self::MAX_FILE_SIZE);
    }

    private function fileExist() {
        return is_file($this->_targetFile);
    }

    public function isImageValide() {
        // test 1
        $statut = $this->testIfImage();
        if (!$statut) {
            $this->_errorMessage = $this->_name . " n'est pas une image";
            return $statut;
        }
        // test 1
        $statut = $this->testFileType();
        if (!$statut) {
            $this->_errorMessage = $this->_name . " n'est pas au bon format";
            return $statut;
        }
        // test 2
        $statut = $this->testFileSize();
        if (!$statut) {
            $this->_errorMessage = $this->_name . " taille de fichier trop importante ".$this->_size;
            return $statut;
        }

        // test 3
        $statut = $this->fileExist();
        if ($statut) {
            $this->_errorMessage = $this->_name . " existe déja";
            return $statut;
        }

        return true;
    }

    public function getName() {
        return $this->_name;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

    public function saveFile() {
        $statut= move_uploaded_file($this->_tmpName, $this->_targetFile);
        if (!$statut) {
            $this->_errorMessage =  "Impossible de sauvegarder ".$this->name;
        }
        return $statut;
    }

}