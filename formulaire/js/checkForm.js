
document.querySelector("#container form button")
    .addEventListener("click",function (event) {
        event.preventDefault()
    //    console.log(event)

        const regexInput = /^[\S]{1,50}$/

        const inputVerif= function () {
            if (this.value.match(regexInput) === null) {
                this.classList.add("error")
                this.previousElementSibling.classList.add("error")
            } else {
                this.classList.remove("error")
                this.previousElementSibling.classList.remove("error")
            }
        }

        let hasError = false

        /** FIRST NAME */
        let firstNameInput=document.querySelector("#firstName")
        let checkfirstName=firstNameInput.value.match(regexInput)
        if (checkfirstName === null) {
            firstNameInput.classList.add("error")
            firstNameInput.previousElementSibling.classList.add("error")
            hasError = true
        }

        firstNameInput.addEventListener('input',function () {
            if (this.value.match(regexInput) === null) {
                this.classList.add("error")
                this.previousElementSibling.classList.add("error")
                hasError = true
            } else {
                this.classList.remove("error")
                this.previousElementSibling.classList.remove("error")
            }
        })

        /** LAST NAME */
        let lastNameInput=document.querySelector("#lastName")
        let checkLastName=lastNameInput.value.match(regexInput)
        if (checkLastName === null) {
            lastNameInput.classList.add("error")
            lastNameInput.previousElementSibling.classList.add("error")
            hasError = true
        }
        lastNameInput.addEventListener('input',inputVerif)

        /** MESSAGE */
        let messageInput=document.querySelector("#message")
        if (messageInput.value === '') {
            messageInput.classList.add("error")
            messageInput.previousElementSibling.classList.add("error")
            hasError = true
        }

        messageInput.addEventListener('input',function () {
            if (this.value === '') {
                this.classList.add("error")
                this.previousElementSibling.classList.add("error")
                hasError = true
            } else {
                this.classList.remove("error")
                this.previousElementSibling.classList.remove("error")
            }
        })

        if (!hasError) {
            document.querySelector("#container form")
                .submit()
        }


    })


