<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Saisie d'un formulaire</title>
    <link rel="stylesheet" href="css/formulaire.css">
    <script src='js/cours.js' async></script>

</head>
<body>
    <section id="container">
        <h1>Formulaire de commentaires</h1>
        <form method="post" >
            <div>
                <label for="firstName" >Prénom <span>*</span></label>
                <input type="text" id="firstName" name="firstName" placeholder="Votre prénom" >
            </div>
            <div>
                <label for="lastName">Nom <span>*</span></label>
                <input type="text" id="lastName" name="lastName"  placeholder="Votre nom">
            </div>
            <div>
                <label for="message">Le message <span>*</span></label>
                <textarea id="message" name="message" cols="30" row="10"></textarea>
            </div>
            <button type="submit" id="btn">Enregistrer</button>
        </form>
    </section>
</body>
</html>
